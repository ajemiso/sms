from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class Person(models.Model):
    '''
    Person model - extends existing Django User Model
    '''
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    username = models.CharField(max_length=128)
    password = models.CharField(max_length=256)
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    joined_at = models.DateTimeField(auto_now=True)


class Message(models.Model):
    '''
    Message model - includes foreign key reverse lookup for Person model
    '''
    content = models.TextField(max_length=1600)
    sent_time = models.DateTimeField(auto_now=True)
    sent_to_phone = models.CharField(max_length=12)
    TWILIO_NUM = models.CharField(max_length=12)
    user = models.ForeignKey(Person, related_name='messages')

    def __str__(self):
        return "{} {}".format(self.content, self.sent_time) 


