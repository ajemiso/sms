from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from private.keys import TWILIO_SID, TWILIO_TOKEN, TWILIO_NUM
from sms.models import Message, Person
from twilio.rest import TwilioRestClient


def home(request):
    '''
    Returns homepage, passes log of messages
    '''
    messages = Message.objects.filter()
    return render(request, 'sms.html', {'logger':messages})

@login_required
@csrf_exempt
def send(request):
    '''
    sends message(s) via received POST request using Twilio library
    '''
    if request.method == 'POST':
        user = request.user.person
        send_to_phone = request.POST['send_to_phone']
        content = request.POST['message']

        client = TwilioRestClient(TWILIO_SID, TWILIO_TOKEN)

        twilio_message = client.messages.create(to=send_to_phone,
                  from_=TWILIO_NUM, body=content)
        
        message = Message.objects.create(content=content, sent_to_phone=send_to_phone,
        TWILIO_NUM=TWILIO_NUM, user=user)
        
        message.save()
    
    return render(request, 'sms.html', {})



