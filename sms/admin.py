from django.contrib import admin
from sms.models import Message, Person


# Registering models
admin.site.register(Message)
admin.site.register(Person)
